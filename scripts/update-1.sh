#!/usr/bin/bash

#sed -i -e 's/\r$//' 1.sh

grp=0

docker service update \
  --image 65.108.146.200:8888/qa-worker:2 \
  --env-add GENERATION=2 \
  --update-parallelism=4 \
  --update-order=stop-first \
  --update-failure-action=rollback \
  --rollback-order=stop-first \
  qa_worker-0

docker service update \
  --image 65.108.146.200:8888/qa-gateway:2 \
  --env-add GENERATION=2 \
  --update-order=stop-first\
  --update-failure-action=rollback \
  --rollback-order=stop-first \
  qa_gateway-0