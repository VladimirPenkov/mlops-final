#!/usr/bin/bash

#sed -i -e 's/\r$//' 1.sh
cd ../gateway ;

docker build -t 65.108.146.200:8888/qa-gateway:1 .;
docker push 65.108.146.200:8888/qa-gateway:1;

cd ../model-worker ;

docker build -t 65.108.146.200:8888/qa-worker:1 .;
docker push 65.108.146.200:8888/qa-worker:1;

cd ../script;