import os
from time import sleep
import random
import sys
import signal
import redis
# import numpy as np
from flask import Flask, request, jsonify
import socket
import asyncio
from apscheduler.schedulers.background import BackgroundScheduler


hostname = socket.gethostname()

EMB_SIZE = 2

GEN = os.environ.get('GENERATION', '0')
PART = os.environ.get('INDEX_PART', '0')
DATA_PATH = os.environ.get('DATA_PATH', '')
REDIS_PASS = os.environ.get('REDIS_PASS', '')
PORT = 5000

redis_host = 'qa_redis'
redis_password = os.environ.get('REDIS_PASS', REDIS_PASS)
redis_port = 6379

rnd = random.randint(0, 1000)


# Для эмуляции работы с данными поставим костыли. идентифицировать работу будем по "имени" воркера, которое он будет писать в ответ.
def my_name():
    return '-'.join(['worker', 'G'+GEN, 'Part'+PART, hostname])

print('Im worker ' + my_name())


def signal_handler(sig, frame):
    print(my_name() + 'Завершаюсь по сигналу')
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)


quality_factor = 1.  # quality_factor < 1 allows to downgrade quality and upgrade speed

print(my_name() + ' starting')

rconn = redis.Redis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)


def refresh_redis():
    try:
        rconn.set(':'.join(['worker', GEN, hostname]), PART, ex=10)
    except:
        print('redis refresh oops  ', ':'.join(['worker', GEN, hostname]), PART)
    return


scheduler = BackgroundScheduler()
job = scheduler.add_job(refresh_redis, 'interval', seconds=5)
scheduler.start()


app = Flask(__name__)


def knn_predict(emb, k: int = 40):
    pretenders = [random.randint(0, 10**6) for i in range(0, k)]
    sleep(0.1 * quality_factor)
    return pretenders


def final_predict(emb, pretenders: list):
    best_match = 'dummy'
    # sleep(0.5 * quality_factor)
    return best_match


def find_answer(emb):
    answer_pretenders = knn_predict(emb)
    best_answer = final_predict(emb, answer_pretenders)
    return best_answer


@app.route('/query')
def query():
    # embedding = request.args.get('embedding')
    embedding = None
    answer = my_name() + ' says: ' + find_answer(embedding)
    return jsonify({'answer': answer})


# костыль для отладки в тестовом контуре.
@app.route('/part')
def part():
    global PART
    PART = request.args.get('part')
    return jsonify({'answer': PART})


@app.route('/health')
def health():
    return my_name() + " is fine"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT, debug=False)
