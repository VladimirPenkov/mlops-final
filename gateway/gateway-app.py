import os
import json
import random
import sys
import signal
import numpy as np
import redis
import requests
from flask import Flask, request, jsonify
import socket
from apscheduler.schedulers.background import BackgroundScheduler

hostname = socket.gethostname()

GEN = os.environ.get('GENERATION', '0')
DATA_PATH = os.environ.get('DATA_PATH', '')
REDIS_PASS = os.environ.get('REDIS_PASS', '')
PORT = 8080
MY_NAME = '-'.join(['gateway', 'G'+GEN, hostname])
WORKER_PORT = 5000

redis_host = 'qa_redis'
redis_password = os.environ.get('REDIS_PASS', REDIS_PASS)
redis_port = 6379

rconn = redis.Redis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)

workers = dict()

EMB_SIZE = 2


def signal_handler(sig, frame):
    print(MY_NAME + 'Завершаюсь по сигналу')
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)


def refresh_redis_keys():
    # получаем ключи воркеров своего поколения
    keys = rconn.keys(f'worker:'+str(GEN)+':*')
    _workers = {}
    try:
        for key in keys:
            idx = rconn.get(key)
            host = key.split(':')[2]
            if idx in _workers.keys():
                _workers[idx].append(host)
            else:
                _workers[idx] = [host]
        print(_workers)
        global workers
        workers = _workers
    except Exception as e:
        print(' Redis ooops:' + str(e))


scheduler = BackgroundScheduler()
job = scheduler.add_job(refresh_redis_keys, 'interval', seconds=5)
scheduler.start()


def select_worker(cluster):
    return random.choice(workers[cluster])


def question_to_embedding(question: str):
    return np.zeros(EMB_SIZE)


def cluster_idx(embedding):
    nearest = random.choice([key for key in workers])
    # todo roundrobin
    return nearest


app = Flask(__name__)


@app.route('/query')
def query():
    question = request.args.get('question')
    print('question:', question)
    embedding = question_to_embedding(question)
    cluster = cluster_idx(embedding)
    payload = {'embedding': embedding}
    worker = select_worker(cluster)
    addr = 'http://' + worker + ':' + str(WORKER_PORT)
    print(addr)
    answer = None
    try:
        response = requests.get(addr + '/query', params=payload).json()
        answer = response['answer']
    except Exception as e:
        print('oops querying worker ' + worker + ':' + str(e))

    return jsonify({'answer': answer})


@app.route('/health')
def health():
    return "I'm gateway, I'm fine"

@app.route('/xxx', methods=['GET'])
def xxx():
    addr = request.args.get('addr')
    if addr == '*':
        response = '*'
    else:
        response = str(requests.get(addr).text)
    print(response)
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT, debug=False)
